﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    //[SerializeField] private Canvas endPanel;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        /*if (collision.gameObject.name == "Bullet(Clone)")
        {
            Time.timeScale = 0;
            endPanel.enabled = !endPanel.enabled;
        }*/
    }
    
    public void ExitTheGame()
    {
        Application.Quit();
    }
}
