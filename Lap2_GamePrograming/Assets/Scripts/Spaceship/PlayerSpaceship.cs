﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Unity.Mathematics;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship , IDamagable
    {
        public event Action OnExploded;

        //Add Code sound fire & Die Boom
        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip playerDieSound;
        [SerializeField] private float playerDieSoundVolume = 0.3f;
        [SerializeField] private bool Cooldown = true;
       
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");

            //Add Code sound fire 
            Debug.Assert(playerFireSound != null, "playerFireSound cannot be null");
        }

        public void Init(int hp , float speed)
        {
            base.Init(hp, speed, defaultBullet , bulletUnti);
        }

        public override void Fire()
        {
            //Add Code sound fire 
            AudioSource.PlayClipAtPoint(playerFireSound, Camera.main.transform.position , playerFireSoundVolume);

            var bullet = Instantiate(defaultBullet, gunPosition.position, gunPosition.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(gunPosition.up * 10, ForceMode2D.Impulse);
        }

        public override void FireUnti()
        {
            StartCoroutine(Unitmate());
            
        }

         

        private IEnumerator Unitmate()
        {
            if (Cooldown)
            {
                //Add Code sound fire 
                AudioSource.PlayClipAtPoint(playerFireSound, Camera.main.transform.position, playerFireSoundVolume);
                Cooldown = false;
                var bulletunti = Instantiate(bulletUnti, gunPosition.position, gunPosition.rotation);

                Rigidbody2D rb = bulletunti.GetComponent<Rigidbody2D>();
                rb.AddForce(gunPosition.up * 20, ForceMode2D.Impulse);
                Cooldown = false;
                yield return new WaitForSeconds(3);
                Cooldown = true;
            }
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Exploded();
        }

        public void Exploded()
        {
            //Add Code sound Boom 
            AudioSource.PlayClipAtPoint(playerDieSound, Camera.main.transform.position, playerDieSoundVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}
