﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;

        //[SerializeField] private int Hp;

        //Add Code sound fire & Die Boom
        [SerializeField] private AudioClip enemyFireSound;
        [SerializeField] private float enemyFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip enemyDieSound;
        [SerializeField] private float enemyDieSoundVolume = 0.3f;

        

        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet , bulletUnti);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }

            Exploded();
        }

        public void Exploded()
        {
            //Add Code sound Boom 
            AudioSource.PlayClipAtPoint(enemyDieSound, Camera.main.transform.position, enemyDieSoundVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                //Add Code sound fire 
                AudioSource.PlayClipAtPoint(enemyFireSound, Camera.main.transform.position, enemyFireSoundVolume);

                var bullet = Instantiate(defaultBullet, gunPosition.position , gunPosition.rotation);
                Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                rb.AddForce(-gunPosition.up * 8, ForceMode2D.Impulse);

                fireCounter = 0;
            }
        }
    }
}
