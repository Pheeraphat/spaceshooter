﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Spaceship
{
    public abstract class BaseSpaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet;
        [SerializeField] protected Bullet bulletUnti;
        [SerializeField] protected Transform gunPosition;

        public event Action Exploded; 

        public int Hp { get; protected set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }
        public Bullet BulletUntimate { get; private set; }

        protected virtual void Init(int hp , float speed , Bullet bullet , Bullet bulletuntimate)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
            BulletUntimate = bulletuntimate;
        }
        public virtual void Fire()
        {
            //TODO Implement this later
        }

        public virtual void FireUnti()
        {
            //TODO Implement this later
        }
    }
}
