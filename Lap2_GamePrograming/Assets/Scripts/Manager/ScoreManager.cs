﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Spaceship;
using Singleton;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        //public EnemySpaceship enemySpaceship;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;
        private int playerScore;
        private GameManager gameManager;

        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            //GameManager.Instance.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score )
        {
            scoreText.text = $"Score: {score}";
            playerScore = score;
        }

        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");
        }

        private void OnRestarted()
        {
            finalScoreText.text = $"Player Score : {playerScore}";
            this.gameManager = gameManager;
            this.gameManager.OnRestarted -= OnRestarted;
            //GameManager.Instance.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }

        /*private void scoreSet()
        {
            SetScore(1);
        }

        public void Update()
        {
           if(enemySpaceship.Hp <= 0)
            {
                scoreSet();
            }
        }*/
    }
}
