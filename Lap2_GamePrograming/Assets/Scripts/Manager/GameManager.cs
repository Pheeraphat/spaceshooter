﻿using System.Collections;
using System.Collections.Generic;
using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Singleton;


namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        [SerializeField] private Text textLevel;
        [SerializeField] private Text textWin;
        [SerializeField] private Text textLose;
        [SerializeField] private Button startButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button exitButton;
        [SerializeField] private Button nextButton;
        [SerializeField] private RectTransform dialog;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        [SerializeField] private int scoreSave = 0;
        [SerializeField] private int CountSpawnEnemys;
        private GameObject temp;
        public Transform prefab;

        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(exitButton != null, "exitButton cannot be null");
            Debug.Assert(nextButton != null, "nextButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(textLevel != null, "text cannot be null");
            Debug.Assert(textWin != null, "text cannot be null");
            Debug.Assert(textLose != null, "text cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceship hp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

            startButton.onClick.AddListener(OnStartButtonClicked);
            restartButton.onClick.AddListener(OnStartButtonClicked);
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            textLevel.gameObject.SetActive(false);
            startButton.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(false);
            exitButton.gameObject.SetActive(false);
            nextButton.gameObject.SetActive(false);
            textWin.gameObject.SetActive(false);
            textLose.gameObject.SetActive(false);
            StartGame();

        }

        private void StartGame()
        {
            scoreManager = GetComponent<ScoreManager>();
            scoreManager.Init(this);
            DestroyRemainingShip();
            SpawnPlayerSpaceship();
            //SpawnEnemySpaceship();
            StartCoroutine(SpawnEnemySpaceship());
            
            
        }

        private void StartAgain()
        {
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }

        private void SpawnPlayerSpaceship()
        {
            
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            textLose.gameObject.SetActive(true);
            Restart();
            
        }

        private IEnumerator SpawnEnemySpaceship()
        {
            EnemySpaceship[] enemyships = new EnemySpaceship[CountSpawnEnemys];

            for (int i = 0; i < CountSpawnEnemys; i++)
            {
                
                enemyships[i] = Instantiate(enemySpaceship);
                enemyships[i].Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
                enemyships[i].OnExploded += OnEnemySpaceshipExploded;
                enemyships[i].gameObject.SetActive(false);
            }

            for (int n = 0; n < CountSpawnEnemys; n++)
                {
                    yield return new WaitForSeconds(1.0f);
                    enemyships[n].gameObject.SetActive(true);
                }
            
            
            
        }

        private void OnEnemySpaceshipExploded()
        {
            scoreSave++;
            GameObject[] temp = GameObject.FindGameObjectsWithTag("Enemy");
            scoreManager.SetScore(scoreSave);
            //temp = GameObject.FindGameObjectsWithTag("Enemy");
            if (scoreSave >= CountSpawnEnemys)
            {
                
                Restart();
                textWin.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(true);
                scoreSave = 0;
            }
            else
            {
                
            }
            

        }

        private void Restart()
        {
            scoreSave = 0;
            DestroyRemainingShip();
            textLevel.gameObject.SetActive(false);
            dialog.gameObject.SetActive(true);
            startButton.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(true);
            exitButton.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShip()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }

            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }
    }
}


