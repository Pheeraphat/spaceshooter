﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;
using Manager;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private Transform playerTransform;
        [SerializeField] private float enemyShipSpeed = 5;
        [SerializeField] private Renderer playerRenderer;
        [SerializeField] private float chasingThresholdDistance;
        private Rigidbody2D rb;
        public GameObject temp;

        private Renderer enemyRenderer;

        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void Awake()
        {
            enemyRenderer = GetComponent<Renderer>();
        }

        void Update()
        {
            FindPlayer();
            MoveToPlayer();
            enemySpaceship.Fire();
        }

        private void OnDrawGizmos()
        {
            CollisionDebug();
        }

        private void CollisionDebug()
        { 
            if (enemyRenderer != null && playerRenderer != null)
            {
                if(intersectAABB(enemyRenderer.bounds , playerRenderer.bounds))
                {
                    Gizmos.color = Color.red;
                }
                else
                {
                    Gizmos.color = Color.white;
                }
                
                Gizmos.DrawWireCube(enemyRenderer.bounds.center, 2 * enemyRenderer.bounds.extents);
                Gizmos.DrawWireCube(playerRenderer.bounds.center, 2 * playerRenderer.bounds.extents);
            }
        }

        private bool intersectAABB(Bounds a , Bounds b)
        {
            return ((a.min.x <= b.max.x && a.max.x >= b.min.x) && (a.min.y <= b.max.y && a.max.y >= b.min.y));
        }

        private void MoveToPlayer()
        {
            //Move Y

            Vector3 enemyPosition = transform.position;
            Vector3 playerPosition = playerTransform.position;
            enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position
            
            Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
            Vector3 directionToTarget = vectorToTarget.normalized;
            Vector3 velocity = directionToTarget * enemyShipSpeed;
                       
            float distanceToTarget = vectorToTarget.magnitude;

            if (distanceToTarget > chasingThresholdDistance)
            {
                // transform.Translate(velocity * Time.deltaTime);
                rb.velocity = velocity;
            }
        }
        
        private void FindPlayer()
        {
            temp = GameObject.FindGameObjectWithTag("Player");
            if(temp == null)
            {
                Destroy(this.gameObject);
            }
            playerRenderer = temp.GetComponent<Renderer>();
            playerTransform = temp.GetComponent<Transform>();
        }
    }    
}

