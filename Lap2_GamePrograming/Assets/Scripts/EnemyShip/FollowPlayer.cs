﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private Renderer playerRenderer;
    public GameObject temp;
    [SerializeField] private int angleoffset;
    // Start is called before the first frame update
    void Update()
    {
        FindPlayer();
        
        var angle = Mathf.Atan2(playerTransform.position.y , playerTransform.position.x) * Mathf.Rad2Deg + angleoffset;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }

    private void FindPlayer()
    {
        temp = GameObject.FindGameObjectWithTag("Player");
        playerRenderer = temp.GetComponent<Renderer>();
        playerTransform = temp.GetComponent<Transform>();
    }
}
