﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel3 : MonoBehaviour
{
    public void NextLevel3Click()
    {

        SceneManager.LoadScene("Level3Boss");
    }
}
