﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private Rigidbody2D rigidbody2D;

        //Add Audio sorue
        //private AudioSource bulletAudio;
        public void Init(Vector2 direction)
        {
            Move(direction);
        }

        private void Awake()
        {
            Debug.Assert(rigidbody2D != null, "rigidbody2D cannot be null");

            //add Audio soure 
            /*bulletAudio = GetComponent<AudioSource>();
            bulletAudio.Play();*/
        }

        private void Move(Vector2 direction)
        {
            rigidbody2D.velocity = direction * speed;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Debug.Log("enemy fire your");
            }

            else if (other.CompareTag("Enemy"))
            {
                Debug.Log("Player fire Enemy");
            }
            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
            
        }

        public void Update()
        {
            Destroy(gameObject, 2);
        }
    }
}
