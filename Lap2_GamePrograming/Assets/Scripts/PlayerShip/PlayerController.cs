﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float playerShipSpeed = 10;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        //public Camera cam;
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;
        private float minX;
        private float maxX;
        private float minY;
        private float maxY;

        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
        }

        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
            inputActions.Player.FireUnti.performed += OnFireUnti;
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            playerSpaceship.Fire();
        }

        private void OnFireUnti(InputAction.CallbackContext obj)
        {
            playerSpaceship.FireUnti();
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
            /*if (obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }

            if (obj.canceled)
            {
                movementInput = Vector2.zero;
            }*/
        }
        private void Update()
        {
            Move2();
            //faceMousePosition();
        }

        private void Move()
        {
            var inPutVelocity = movementInput * playerSpaceship.Speed;

            var newPosition = transform.position;

            newPosition.x = transform.position.x + inPutVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + inPutVelocity.y * Time.smoothDeltaTime;

            newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);

            transform.position = newPosition;
        }

        private void Move2()
        {
            var inputDirection = movementInput.normalized;
            var inPutVelocity = inputDirection * playerShipSpeed;

            var gravityForce = new Vector2(0, -2);
            //var blackholeForce = new Vector2(2, 0);

            //var finalVelocity = inPutVelocity + gravityForce + blackholeForce;
            var finalVelocity = inPutVelocity;

            /*Debug.DrawRay(transform.position, inPutVelocity, Color.blue);
            Debug.DrawRay(transform.position, gravityForce, Color.red);
            Debug.DrawRay(transform.position, blackholeForce, Color.yellow);
            Debug.DrawRay(transform.position, finalVelocity, Color.white);*/


            var newXPos = transform.position.x + finalVelocity.x * Time.deltaTime;
            var newYPos = transform.position.y + finalVelocity.y * Time.deltaTime;

            newXPos = Mathf.Clamp(newXPos, minX, maxX);
            newYPos = Mathf.Clamp(newYPos, minY, maxY);

            transform.position = new Vector2(newXPos, newYPos);
        }


        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, "Main Camera cannot be null");

            var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");

            var offset = spriteRenderer.bounds.size;
            minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }

        void faceMousePosition()
        {
            

        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }
}
