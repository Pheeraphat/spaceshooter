﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceMouseController : MonoBehaviour
{
    [SerializeField] private int angleoffset;
    // Update is called once per frame
    void Update()
    {
        var dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + angleoffset;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        /*Vector3 mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

            Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
            transform.up = direction;*/
    }
}
